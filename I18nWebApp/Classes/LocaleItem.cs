﻿namespace I18nWebApp.Classes
{
	/// <summary>
	/// Список поддерживаемых локалей
	/// </summary>
	public class LocaleItem
	{
		#region Constructors

		public LocaleItem()
		{
		}

		public LocaleItem(System.Xml.XPath.XPathNavigator node)
		{
			Name = node.GetAttribute("name", string.Empty);
			Value = node.GetAttribute("value", string.Empty);
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// Имя локали
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Код локали
		/// </summary>
		public string Value { get; set; }

		#endregion Properties
	}
}