﻿namespace I18nWebApp.Classes.I18N
{
	using I18nWebApp.Classes;
	using System.Collections.Generic;

	/// <summary>
	/// Интерфейс локализации
	/// </summary>
	public interface II18N
	{
		/// <summary>
		/// Имя текущей локали
		/// </summary>
		string CultureName { get; }

		/// <summary>
		/// Список локалей
		/// </summary>
		/// <returns></returns>
		IList<LocaleItem> I18NList();

		/// <summary>
		/// Перевести
		/// </summary>
		/// <param name="key"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		string T(string key, params object[] args);

		/// <summary>
		/// Путь к файлу описания локализаций
		/// </summary>
		string XmlInfoPath { get; }

		/// <summary>
		/// Путь к файлу перевода
		/// </summary>
		string XmlPath { get; }

		/// <summary>
		/// Translator
		/// </summary>
		TDelegate Localizer { get; }

		/// <summary>
		/// Load translation
		/// </summary>
		void LoadTranslation();

		System.Globalization.CultureInfo CultureInfo { get; set; }
	}
}
