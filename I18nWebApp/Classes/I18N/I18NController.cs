﻿namespace I18nWebApp.Classes.I18N
{
	using System.Globalization;
	using System.Web.Mvc;
	using System.Web.Routing;

	/// <summary>
	/// <para>
	/// Модифицированный контроллер, поддерживающий определение локали (из маршрута).
	/// </para>
	/// <para>Содержит ряд вспомогательных методов и свойст, использующихся в приложении</para>
	/// </summary>
	public abstract class I18NController : Controller
	{
		#region

		TDelegate translator = I18N.NullLocalizer;
		private II18N ii18nPrvider = new I18N();

		#endregion

		#region Properties

		/// <summary>
		/// Локаль
		/// </summary>
		public string CultureName { get; private set; }

		/// <summary>
		/// Сервис локализации строк
		/// </summary>
		public TDelegate T
		{
			get { return I18nProvider.Localizer; }
		}

		public II18N I18nProvider
		{
			get { return ii18nPrvider; }
		}

		#endregion Properties

		#region Methods

		protected override void Initialize(RequestContext requestContext)
		{
			string oldCulture = System.Globalization.CultureInfo.CurrentCulture.ToString().ToLower();
			CultureInfo ci = CultureInfo.GetCultureInfo(oldCulture);

			try
			{
				CultureName = requestContext.RouteData.Values["language"].ToString();
				ci = CultureInfo.GetCultureInfo(CultureName);
			}
			catch (System.Globalization.CultureNotFoundException)
			{
				//some loging stuff goes here

				CultureName = oldCulture;
				//fix wrong culture in route
				requestContext.RouteData.Values["language"] = oldCulture;
				ci = CultureInfo.GetCultureInfo(oldCulture);
			}
			finally
			{
				System.Threading.Thread.CurrentThread.CurrentCulture = ci;
				System.Threading.Thread.CurrentThread.CurrentUICulture = ci;

				if (I18nProvider.CultureInfo != ci)
				{
					I18nProvider.CultureInfo = ci;
					I18nProvider.LoadTranslation();
				}
			}

			base.Initialize(requestContext);
		}

		#endregion Methods
	}
}