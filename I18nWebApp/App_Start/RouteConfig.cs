﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace I18nWebApp
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes, string languageConstraint = "(ru-RU|en-US)")
		{
			string currentLocale = System.Globalization.CultureInfo.CurrentCulture.ToString().ToLower();
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

			routes.MapRoute(
				"Default",
				"{language}/{controller}/{action}/{id}",
				new { controller = "Home", action = "Index", language = currentLocale, id = string.Empty },
				new { language = languageConstraint }
			);
		}
	}
}